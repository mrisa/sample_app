class StaticPagesController < ApplicationController
  def home
  end
  
  def current_user
  end

  def help
  end

  def about
  end

  def contact
  end
end
